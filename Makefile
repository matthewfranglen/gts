.PHONY: clean data requirements test-environment .FORCE

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PROJECT_NAME = GTS

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Run Jupyter Lab
edit: requirements
	poetry run jupyter lab --no-browser --ip 0.0.0.0

## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete

## Train a model
train: requirements
	PYTHONPATH=code/BertModel/ poetry run python -m main \
			   --task pair --mode train --dataset res14 --prefix data/ \
			   --bert_model_path bert-base-uncased --bert_tokenizer_path bert-base-uncased

## Evaluate trained model
evaluate: requirements
	PYTHONPATH=code/BertModel/ poetry run python -m main \
			   --task pair --mode test --dataset res14 --prefix data/ \
			   --bert_model_path ./savemodel --bert_tokenizer_path bert-base-uncased

## Evaluate on the model downloaded from google drive, assumed to be in pretrained
evaluate-pretrained: requirements
	PYTHONPATH=code/BertModel/ poetry run python -m main \
			   --task pair --mode test --dataset res14 --prefix data/ \
			   --bert_model_path ./pretrained --bert_tokenizer_path bert-base-uncased

## Install Python Dependencies
requirements: .make/requirements

.make/requirements: pyproject.toml
	poetry install
	if [ ! -e .make ]; then mkdir .make; fi
	touch .make/requirements

.FORCE:

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
